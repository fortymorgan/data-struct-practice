const { createMap } = require('../Map.old');

describe('map.old tests', () => {
  const map1 = createMap('one', 1);
  const map312 = createMap('three', 3);
  map312.put('one', 1);
  map312.put('two', 2);

  it('should extract data at key \'one\'', () => {
    expect(map1.get('one')).toBe(1);
  });

  it('should throw error on put value at key \'one\'', () => {
    expect(() => map1.put('one', 1)).toThrowError('Key is already exists');
  });

  it('should throw error on get value at key \'two\'', () => {
    expect(() => map1.get('two')).toThrowError('Key doesn\'t exist');
  });

  it('should extract data at key \'two\'', () => {
    expect(map312.get('two')).toBe(2);
  });

  it('should throw error on put value at key \'one\'', () => {
    expect(() => map312.put('one', 1)).toThrowError('Key is already exists');
  });

  it('should throw error on get value at key \'four\'', () => {
    expect(() => map312.get('four')).toThrowError('Key doesn\'t exist');
  });
});
