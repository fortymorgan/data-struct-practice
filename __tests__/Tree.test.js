const { create } = require('../Tree');

describe('tree tests', () => {
  const treeSample1 = create('foo', [
    create('bar'),
  ]);

  const treeSample2 = create('foo', [
    create('bar', [
      create('qux'),
    ]),
    create('baz'),
  ]);
  
  const treeSample3 = create('foo', [
    create('baz'),
    create('bar'),
  ]);

  const treeSample4 = create('foo', [
    create('bar'),
    create('baz', [
      create('qux'),
    ]),
  ]);

  it('should create a create', () => {
    const tree = create('foo');
    expect(tree).toEqual(create('foo'));
    expect(tree.depth()).toBe(0);
  });

  
  it('should be with one child', () => {
    const tree = create('foo');
    const bar = tree.addChild('bar');
    expect(tree).toEqual(treeSample1);
    expect(tree.isLeaf()).toBe(false);
    expect(bar).toEqual(create('bar'));
    expect(bar.isLeaf()).toBe(true);
  });

  it('should be with two children', () => {
    const tree = create('foo');
    tree.addChild('bar').addChild('qux');
    tree.addChild('baz');

    const qux = tree.find('qux');

    const baz = tree.getChildOnIdx(1);

    expect(tree).toEqual(treeSample2);
    expect(qux).toEqual(create('qux'));
    expect(baz).toEqual(create('baz'));
    expect(tree.depth()).toBe(2);
  });

  it('should add a first child', () => {
    const tree = create('foo');
    tree.addChild('bar');
    tree.addChildOnIdx('baz', 0);

    expect(tree).toEqual(treeSample3);
  });

  it('should add a subtree', () => {
    const tree = create('foo');
    tree.addChild('bar');

    const subtree = create('baz');
    subtree.addChild('qux');

    tree.addSubTree(subtree);

    expect(tree).toEqual(treeSample4);
  });
});
