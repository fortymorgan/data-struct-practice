export class BST<A> {
  _left: BST<A> | null;
  _right: BST<A> | null;

  constructor(private _node: A, private _compare: (node: A) => string | number) {
    this._left = null;
    this._right = null;
  }

  node(): A {
    return this._node;
  }

  left(): BST<A> | null {
    return this._left;
  }

  right(): BST<A> | null {
    return this._right;
  }

  size(acc?: number): number {
    if (acc === undefined) {
      acc = 1;
    }
    if (this._right !== null && this._left !== null) {
      return acc + this._left.size() + this._right.size();
    }
    if (this._right !== null) {
      return this._right.size(acc + 1);
    }
    if (this._left !== null) {
      return this._left.size(acc + 1);
    }

    return acc;
  }

  assignToLeft(child: BST<A> | null): void {
    this._left = child;
  }

  assignToRight(child: BST<A> | null): void {
    this._right = child;
  }

  isLeaf(): boolean {
    return this.left() === null && this.right() === null;
  }

  getComparable(node: A): string | number {
    return this._compare(node);
  }

  add(value: A): void {
    if (this.getComparable(value) < this.getComparable(this.node())) {
      if (this._left === null) {
        this.assignToLeft(createBST(value, this._compare));
      } else {
        this._left.add(value);
      }
    } else if (this.getComparable(value) > this.getComparable(this.node())) {
      if (this._right === null) {
        this.assignToRight(createBST(value, this._compare));
      } else {
        this._right.add(value);
      }
    } else {
      throw new Error('Key is already exists');
    }
  }

  find(key: string): A {
    if (key === this.getComparable(this.node())) {
      return this.node();
    } else if (key < this.getComparable(this.node())) {
      if (this._left === null) {
        throw new Error('Key doesn\'t exist');
      }
      return this._left.find(key);
    } else {
      if (this._right === null) {
        throw new Error('Key doesn\'t exist');
      }
      return this._right.find(key);
    }
  }

  remove(key: string | number, assignToParent?: (child: BST<A> | null) => void): void | null {
    if (key < this.getComparable(this.node())) {
      if (this._left === null) {
        throw new Error('Key doesn\'t exist');
      }
      this._left.remove(key, this.assignToLeft.bind(this));
    } else if (key > this.getComparable(this.node())) {
      if (this._right === null) {
        throw new Error('Key doesn\'t exist');
      }
      this._right.remove(key, this.assignToRight.bind(this));
    } else {
      return this.removeProcess(assignToParent);
    }
  }

  findLeftOne(node: BST<A>): BST<A> {
    if (node._left === null) {
      return node;
    } else {
      return node.findLeftOne(node._left)
    }
  }

  removeProcess(assignToParent?: (child: BST<A> | null) => void): void | null {
    if (assignToParent !== undefined) {
      if (this.isLeaf()) {
        assignToParent(null);
      } else if (this._left === null) {
        assignToParent(this.right());
      } else if (this._right === null) {
        assignToParent(this.left());
      } else {
        if (this._right.left() === null) {
          this._right.assignToLeft(this.left());
          assignToParent(this._right);
        } else {
          const leftOne = this.findLeftOne(this._right);
          this._node = leftOne.node();
          this._right.remove(this._compare(leftOne.node()), this._right.assignToRight.bind(this._right));
        }
      }
    } else {
      if (this.isLeaf()) {
        return null;
      } else if (this._left !== null && this._right === null) {
        this._node = this._left.node()
        this._right = this._left.right();
        this._left = this._left.left();
      } else if (this._right !== null && this._left === null) {
        this._node = this._right.node()
        this._left = this._right.left();
        this._right = this._right.right();
      } else if (this._right !== null && this._left !== null) {
        if (this._right.left() === null) {
          this._node = this._right.node();
          this._right = this._right.right();
        } else {
          const leftOne = this.findLeftOne(this._right);
          this._node = leftOne.node();
          this._right.remove(this._compare(leftOne.node()), this._right.assignToRight.bind(this._right));
        }
      }
    }
  }
}

export function createBST<A>(value: A, compare: (node: A) => string | number): BST<A> {
  return new BST<A>(value, compare);
}
