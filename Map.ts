import { BST, createBST } from './BST';

export interface Map<B> {
  getItem(key: string): B;
  putItem(key: string, value: B): Map<B>;
  removeItem(key: string): Map<B>;
  size(): number;
}

class EmptyMap<B> implements Map<B> {
  _tree: null;
  constructor() {
    this._tree = null;
  }

  size(): number {
    return 0;
  }

  getItem(key: string): never {
    throw new Error('Storage is empty');
  }

  putItem(key: string, value: B): Map<B> {
    return new TreeMap<B>(key, value);
  }

  removeItem(key: string): never {
    throw new Error('Storage is empty');
  }
}

class TreeMap<B> implements Map<B> {
  _tree: BST<{ key: string, value: B }>;
  constructor(key: string, value: B) {
    const compare: (node: { key: string, value: B }) => string = node => node.key;
    this._tree = new BST({ key, value }, compare);
  }

  size(): number {
    return this._tree.size();
  }

  getItem(key: string): B {
    return this._tree.find(key).value;
  }

  putItem(key: string, value: B): Map<B> {
    this._tree.add({ key, value });
    return this;
  }

  removeItem(key: string): Map<B> {
    const result = this._tree.remove(key);
    if (result === null) {
      return new EmptyMap<B>();
    }
    return this;
  }
}

export function createMap<B>(key?: string, value?: B): Map<B> {
  if (!key || !value) {
    return new EmptyMap<B>();
  }
  return new TreeMap<B>(key, value);
}
