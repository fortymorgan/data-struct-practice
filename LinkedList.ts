export interface LinkedList<A> {
  head(): A;
  tail(): LinkedList<A>;
  isEmpty(): boolean;
  size(): number;
  add(item: A): LinkedList<A>;
  append(item: A): LinkedList<A>;
  addAt(item: A, idx: number): LinkedList<A>;
  getItem(idx: number): A;
  toString(depth?: number): string;
}

export function empty<A>(): LinkedList<A> {
  return new Empty();
}

export function cons<A>(item: A, list: LinkedList<A>): LinkedList<A> {
  return new NonEmpty(item, list);
}

export function list<A>(...items: Array<A>): LinkedList<A> {
  if (items.length === 0) {
    return empty();
  }
  return cons(items[0], list(...items.slice(1)));
}

class Empty<A> implements LinkedList<A> {
  head(): never {
    throw new Error('Head of empty list');
  }

  tail(): never {
    throw new Error('Tail of empty list');
  }

  isEmpty(): boolean {
    return true;
  }

  size(): number {
    return 0;
  }

  add(item: A): LinkedList<A> {
    return cons(item, this);
  }

  append(item: A): LinkedList<A> {
    return cons(item, this);
  }

  addAt(item: A, idx: number): LinkedList<A> {
    if (idx === 0) {
      return cons(item, this);
    }
    throw new Error('Index out of range');
  }

  getItem(idx: number): never {
    throw new Error('Index out of range');
  }

  toString(depth: number = 0): string {
    return depth === 0 ? '[]' : '';
  }
}

class NonEmpty<A> implements LinkedList<A> {
  constructor(private _head: A, private _tail: LinkedList<A>) {}

  head(): A {
    return this._head;
  }

  tail(): LinkedList<A> {
    return this._tail;
  }

  isEmpty(): boolean {
    return false;
  }

  size(): number {
    return 1 + this.tail().size();
  }

  add(item: A): LinkedList<A> {
    return cons(item, this);
  }

  append(item: A): LinkedList<A> {
    return cons(this.head(), this.tail().append(item));
  }

  addAt(item: A, idx: number): LinkedList<A> {
    return idx === 0 ? cons(item, this) : cons(this.head(), this.tail().addAt(item, idx - 1));
  }

  getItem(idx: number): A {
    return idx === 0 ? this.head() : this.tail().getItem(idx - 1);
  }

  toString(depth: number = 0): string {
    const string = `${this.head()}, ${this.tail().toString(depth + 1)}`;
    return depth === 0 ? `[${string.slice(0, string.length - 2)}]` : string;
  }
}
