function empty() {
  return new Empty();
}

function cons(item, list) {
  return new NonEmpty(item, list);
}

function list(...items) {
  if (items.length === 0) {
    return empty();
  }
  return cons(items[0], list(...items.slice(1)));
}

class Empty {
  head() {
    throw new Error('Head of empty list');
  }

  tail() {
    throw new Error('Tail of empty list');
  }

  isEmpty() {
    return true;
  }

  size() {
    return 0;
  }

  add(item) {
    return cons(item, this);
  }

  append(item) {
    return cons(item, this);
  }

  addAt(item, idx) {
    if (idx === 0) {
      return cons(item, this);
    }
    throw new Error('Index out of range');
  }

  getItem() {
    throw new Error('Index out of range');
  }

  toString(depth = 0) {
    return depth === 0 ? '[]' : '';
  }
}

class NonEmpty {
  constructor(_head, _tail) {
    this._head = _head;
    this._tail = _tail;
  }

  head() {
    return this._head;
  }

  tail() {
    return this._tail;
  }

  isEmpty() {
    return false;
  }

  size() {
    return 1 + this.tail().size();
  }

  add(item) {
    return cons(item, this);
  }

  append(item) {
    return cons(this.head(), this.tail().append(item));
  }

  addAt(item, idx) {
    return idx === 0 ? cons(item, this) : cons(this.head(), this.tail().addAt(item, idx - 1));
  }

  getItem(idx) {
    return idx === 0 ? this.head() : this.tail().getItem(idx - 1);
  }

  toString(depth = 0) {
    const string = `${this.head()}, ${this.tail().toString(depth + 1)}`;
    return depth === 0 ? `[${string.slice(0, string.length - 2)}]` : string;
  }
}

module.exports.empty = empty;
module.exports.list = list;
module.exports.cons = cons;
