export class Tree<A> {
  constructor(private _node: A, private _children: Array<Tree<A>> = []) {}

  getNode(): A {
    return this._node;
  }

  getChildren(): Array<Tree<A>> {
    return this._children;
  }

  isLeaf(): boolean {
    return this._children.length === 0;
  }

  preorder(cb: (node: A) => void): void {
    cb(this.getNode());
    this.getChildren().forEach(ch => ch.preorder(cb));
  }

  postorder(cb: (node: A) => void): void {
    this.getChildren().forEach(ch => ch.postorder(cb));
    cb(this.getNode());
  }

  addChild(item: A): Tree<A> {
    const newChild = create(item);
    const newChildren = [...this.getChildren(), newChild];
    this._children = newChildren;
    return newChild;
  }
  
  addChildOnIdx(item: A, idx: number): Tree<A> {
    if (idx > this.getChildren().length) {
      throw new Error('Index out of range');
    }
    const newChild = create(item);
    const currChildren = this.getChildren();
    const newChildren = [...currChildren.slice(0, idx), newChild, ...currChildren.slice(idx)];
    this._children = newChildren;
    return newChild;
  }

  getChildOnIdx(idx: number): Tree<A> {
    if (idx > this.getChildren().length) {
      throw new Error('Index out of range');
    }
    return this.getChildren()[idx];
  }

  addSubTree(tree: Tree<A>) {
    const newChildren = [...this.getChildren(), tree];
    this._children = newChildren;
  }

  find(item: A): Tree<A> {
    const result = this.getNode() === item ? this
      : this.getChildren().reduce((acc, ch) => {
        if (ch.getNode() === item) {
          return ch;
        }
        return ch.isLeaf() ? acc : ch.find(item);
      }, null);
    if (result === null) {
      throw new Error('Item not found');
    }
    return result;
  }

  depth(curr: number = 0): number {
    return this.getChildren().reduce((acc, elem) => {
      const currDepth = elem.isLeaf() ? curr + 1 : elem.depth(curr + 1);
      return acc < currDepth ? currDepth : acc;
    }, curr);
  }
}

export function create<A>(node: A, children: Array<Tree<A>> = []): Tree<A> {
  return new Tree(node, children);
};
