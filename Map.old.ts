export interface BST<A> {
  node(): { key: string, value: A };
  left(): BST<A>;
  right(): BST<A>;
  assignToLeft(key: string, value: A): void;
  assignToRight(key: string, value: A): void;
  put(key: string, value: A): void;
  get(key: string): A;
}

export function createMap<A>(key: string, value: A): BST<A> {
  return new Map(key, value);
}

function error(message: string) {
  throw new Error(message);
}

class Map<A> implements BST<A> {
  _node: { key: string, value: A };
  _left: BST<A>;
  _right: BST<A>;
  constructor(key: string, value: A) {
    this._node = { key, value };
  }

  node(): { key: string, value: A } {
    return this._node;
  }

  left(): BST<A> {
    return this._left;
  }

  right(): BST<A> {
    return this._right;
  }

  assignToLeft(key: string, value: A): void {
    this._left = createMap(key, value);
  }

  assignToRight(key: string, value: A): void {
    this._right = createMap(key, value);
  }

  put(key: string, value: A): void {
    if (key < this.node().key) {
      if (this.left() === undefined) {
        this.assignToLeft(key, value);
      } else {
        this.left().put(key, value);
      }
    } else if (key > this.node().key) {
      if (this.right() === undefined) {
        this.assignToRight(key, value);
      } else {
        this.right().put(key, value);
      }
    } else {
      error('Key is already exists');
    }
  }

  get(key: string): A {
    if (key === this.node().key) {
      return this.node().value;
    } else if (key < this.node().key) {
      if (this.left() === undefined) {
        error('Key doesn\'t exist');
      }
      return this.left().get(key);
    } else {
      if (this.right() === undefined) {
        error('Key doesn\'t exist');
      }
      return this.right().get(key);
    }
  }
}
