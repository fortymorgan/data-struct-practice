const { Map, createMap } = require('../Map');

describe('map on binary search tree test', () => {
  const map = createMap();

  it('should be an empty map', () => {
    expect(map.size()).toBe(0);
    expect(() => map.getItem()).toThrowError('Storage is empty');
    expect(() => map.removeItem()).toThrowError('Storage is empty');
  });

  it('add one item, get one item, check size of map', () => {
    const newMap = map
      .putItem('5', 'five');

    expect(newMap.size()).toBe(1);
    expect(newMap.getItem('5')).toBe('five');
  });

  it('add three items, check size, get not root items, remove root item, check size, get items', () => {
    const newMap = map
      .putItem('5', 'five')
      .putItem('6', 'six')
      .putItem('4', 'four');

    expect(newMap.size()).toBe(3);
    expect(newMap.getItem('4')).toBe('four');
    expect(newMap.getItem('6')).toBe('six');

    newMap.removeItem('5');

    expect(newMap.size()).toBe(2);
    expect(newMap.getItem('4')).toBe('four');
    expect(newMap.getItem('6')).toBe('six');
  });
});
