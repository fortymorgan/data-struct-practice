class Tree {
  constructor(node, children = []) {
    this._node = node;
    this._children = children;
  }

  getNode() {
    return this._node;
  }

  getChildren() {
    return this._children;
  }

  isLeaf() {
    return this._children.length === 0;
  }

  addChild(item) {
    const newChild = new Tree(item);
    const newChildren = [...this.getChildren(), newChild];
    this._children = newChildren;
    return newChild;
  }
  
  addChildOnIdx(item, idx) {
    const newChild = new Tree(item);
    const currChildren = this.getChildren();
    const newChildren = [...currChildren.slice(0, idx), newChild, ...currChildren.slice(idx)];
    this._children = newChildren;
    return newChild;
  }

  getChildOnIdx(idx) {
    return this.getChildren()[idx];
  }

  addSubTree(tree) {
    const newChildren = [...this.getChildren(), tree];
    this._children = newChildren;
  }

  find(item) {
    return this.getNode() === item ? this
      : this.getChildren().reduce((acc, ch) => {
        if (ch.getNode() === item) {
          return ch;
        }
        return ch.isLeaf() ? acc : ch.find(item);
      }, null);
  }

  depth(curr = 0) {
    return this.getChildren().reduce((acc, elem) => {
      const currDepth = elem.isLeaf() ? curr + 1 : elem.depth(curr + 1);
      return acc < currDepth ? currDepth : acc;
    }, curr);
  }
}

module.exports = Tree;
