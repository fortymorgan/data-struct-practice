import { LinkedList, empty, list } from './LinkedList';
import { Tree, create } from './Tree';
import { BST, createMap } from './Map';
 
const a: LinkedList<number> = list(1, 2, 3, 4, 5);

console.log(a.toString());

const qux: Tree<string> = create('qux');
const foo = qux.addChild('foo');
const bar = qux.addChild('bar');
const baz = bar.addChild('baz');

console.log(qux);
console.log(qux.depth());

const map: BST<number> = createMap('e', 5);
map.add('c', 3);
map.add('d', 4);
map.add('b', 2);
map.add('a', 1);
map.add('i', 9);
map.add('h', 8);
map.add('g', 7);
map.add('f', 6);

console.log(map);

const three = map.find('d');
const five = map.find('f');

console.log(three);
console.log(five);
